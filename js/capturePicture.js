var captureImage = () => {
    const player = document.getElementById('player');
    const canvas = document.getElementById('canvas');
    const context = canvas.getContext('2d');
  
    const constraints = {
      video: {facingMode: "environment"}/*,
      function(localMediaStream) {

      },
      function(err) {
        console.log('The following error occurred when trying to use getUserMedia: ' + err);
      }*/
    };

    $('#capture').click(function() {
    $('#canvas').css('display','inherit');
    context.drawImage(player, 0, 0, canvas.width, canvas.height);
      
    // Stop all video streams.
    player.srcObject.getVideoTracks().forEach(track => track.stop());
    $('#player, #capture').css('display','none');

	  var dataURL= canvas.toDataURL();

    $.ajax({
      type: "POST",
      url: "saveImage.php",
      data: {image: dataURL},
      beforeSend: function() {
        $('#beerFormSubmit').prop('disabled',true);
        $('#beerFormSubmit').html("Please Wait for Image...");
      },
      complete: function() {
        $('#beerFormSubmit').prop('disabled',false);
        $('#beerFormSubmit').html("Ready to Submit");
      },
      success: function(data) {
      var pwait = Promise.resolve(data);
      pwait.then( data => {
        responseImageArray = JSON.parse(data);  
        if (responseImageArray[0] == "image_success") {
          $('#captureImagePath').val(responseImageArray[1]);
        } else if (responseImageArray[0] == "image_fail") {
          //Load error message and then exit overlay
        }
        console.log(responseImageArray);
      });
       
    }
    });

    });
  
    
      navigator.mediaDevices.getUserMedia(constraints)
      .then((stream) => {
        // Attach the video stream to the video element and autoplay.
        player.srcObject = stream;
      })
      .catch((err) => {
        console.log("Error caught: " + err.name + ", " + err.message);
        $('#capImageOverlay').css('display','none');
        $('#takeImage').css('display', 'none');
        $('#imageUploadContainer').css('display', 'block');
        $('#imageCapBtnContainer').html("<h4 style='color: #c51541;'>No Camera Detected</h4>");
      });  
}