var checkPassMatch = () => {
    var pass1 = $("#registerPassword").val();
    var pass2 = $("#registerPasswordConfirm").val();
    var passwordMatchYes = true;

    if (pass1 != pass2 && pass1 != "" && pass2 != "") {
        passwordMatchYes = false;
        $("#registerPassword").css("border-color","#f44256"); 
        $("#registerPasswordConfirm").css("border-color","#f44256"); 
        $("#passMsg").html("<p style = 'color: #f44256;'>The passwords do not match!</p>");
    } else if (pass1 != "" && pass2 != ""){
        $("#registerPassword").css("border-color","#00cc77"); 
        $("#registerPasswordConfirm").css("border-color","#00cc77"); 
        $("#passMsg").html("<p style = 'color: #00cc77;'>Passwords Match!</p>");
    } else if (pass1 == "" || pass2 == "") {
        $("#registerPassword").css("border-color","#b2b2b2");
        $("#registerPasswordConfirm").css("border-color","#b2b2b2"); 
        $("#passMsg").html("");
    }
}