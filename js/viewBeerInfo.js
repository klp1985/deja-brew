var viewBeerInfo = (beerID) => {
    $.ajax({
        type: 'POST',
        url: 'getBeerInfo.php',
        data: {beerID: beerID},
        success: function(data) {
            var responseInfoArray = JSON.parse(data);
            
            var fsbeerID = responseInfoArray[0];
            var fsbeerName = responseInfoArray[1];
            var fsbreweryName = responseInfoArray[2];
            var fsbeerRating = responseInfoArray[3];
            var fsbeerNotes = responseInfoArray[4];
            var fsimagePath = responseInfoArray[5];

            $('#viewBeerName').html(fsbeerName);
            $('#viewBreweryName').html(fsbreweryName);
            for(i=0;i<fsbeerRating;i++){
                $('#viewBeerRating').append("<i class='fas fa-beer' aria-hidden='true'></i>");
            }
            $('#viewBeerNotes').html(fsbeerNotes);
            $('#viewBeerImage').attr("src",fsimagePath);
            $('#deleteBtn').attr("href", "deletePage.php?keyid=" + fsbeerID + "&tname=beer")
        },
        error: function() {alert("there was an error")}
    });
}