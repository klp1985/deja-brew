<?php
    class ImageUpload {

      //Properties
          public $filename;
          public $filesize;
          public $filetmp;
          public $filetype;
          public $fullImageFilePath;
          public $username;

      //Setters

            function set_filename($inFilename) {
                $this -> filename = $inFilename;  //Put input value into property
            }

            function set_filesize($inFilesize) {
                $this -> filesize = $inFilesize;  //Put input value into property
            }

            function set_filetmp($inFiletmp) {
                $this -> filetmp = $inFiletmp;  //Put input value into property
            }

            function set_filetype($inFiletype) {
                $this -> filetype = $inFiletype;  //Put input value into property
            }

            function set_username($inUsername) {
                $this -> username = $inUsername;
            }

      //Getters
      		function get_filename() {
      			return $this -> filename;
      		}

            function get_filesize() {
      			return $this -> filesize;
      		}

            function get_filetmp() {
      			return $this -> filetmp;
      		}

            function get_filetype() {
      			return $this -> filetype;
      		}

            function get_fullPath() {
                return $this -> fullImageFilePath;
            }


          //  $imageFileName = $_POST["imageFileName"];
          function uploadFullImage() {

              $fullResultMsg = "";
              if (!is_dir("../images/user_images/".$this->username)){
                  mkdir("../images/user_images/$this->username");
              }
              $sizeError = "";
              $typeError = "";

              $explode = explode('.',$this->filename);

              $file_ext=strtolower(end($explode));

              $extensions= array("jpeg","jpg","png","gif");

              if(in_array($file_ext,$extensions)=== false){
                 $typeError="extension not allowed, please choose a JPG, GIF or PNG file.";
              }

              if($this->filesize > 4194304){
                 $sizeError='File size must be less than 4 MB';
              }

              if(empty($errors)==true){
                 $this->fullImageFilePath = "../images/user_images/".$this->username."/".$this->filename;
                 move_uploaded_file($this->filetmp, "$this->fullImageFilePath");

              }else{
                 $fullResultMsg = $sizeError." ".$typeError;
                 return $fullResultMsg;
              }

        }

    }
?>
