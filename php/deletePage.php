<?php
    session_start();
// Admin has access to delete any content.  Customer has access to delete their own account.
if ($_SESSION['validUser'] == "yes") {
  // Bring in the Delete class
    include 'sqlDelete.php';

    //Gets the primary key and table name
    $primaryKey = $_GET['keyid'];
    $tableName = $_GET['tname'];

    //Create delete object
    $delete = new sqlDelete();

    $delete->set_rowId($primaryKey);
    $delete->set_tableName($tableName);

		$resultMsg = $delete->deleteRowFromDB();
		
		header('Location: dashboard.php');

}
/*
    if ($_SESSION['userRole'] == 3) {  //If customer deletes account immediately log them out and send them to login page.
       session_unset();	//remove all session variables related to current session
  	   session_destroy();	//remove current session

  	   header('Location: login.php');
     }

} else {  // Editors do not have access to delete anything.  Send them and non-valid users back to the login page.
    header('Location: login.php');
}*/
?>
