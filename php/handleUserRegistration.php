<?php
    $exportArray = [];
    $resultMsg = '';
    $resultEmail = '';
    $resultPassword = "";
    $resultPasswordConfirm = "";
    $validForm = true;
    include 'validations.php';

    $validation = new Validations();

    $inPass = $_POST['passValue'];
    $inPassConfirm = $_POST['passConfirmValue'];
    $inEmail = $_POST['emailValue'];
    
    $validation->set_email($inEmail);
    $validation->set_password($inPass);
    $validation->set_passwordConfirm($inPassConfirm);
    $validation->set_validForm($validForm);

    $resultEmail = $validation->validateEmail();
    $resultPassword = $validation->validatePassword();
    $resultPasswordConfirm = $validation->validatePasswordConfirm();

    $validForm = $validation->get_validForm();
    $inEmail = $validation->get_email();
    $inPassword = $validation->get_password();
    $inPasswordConfirm = $validation->get_passwordConfirm();

    $getUsername = explode("@", $inEmail);
    $usernameToDB = $getUsername[0];
    $passToDB = password_hash($inPass,PASSWORD_DEFAULT);

    if ($validForm) { //If form passes validation
        
        include "connectPDO.php";  //Connects to the database and inserts the data into the users table.

        $sqlSearch = "SELECT user_email FROM users WHERE user_email = :email_entry;";

        $stmt = $conn->prepare($sqlSearch);
        $stmt->bindParam(':email_entry', $inEmail);
        $stmt->execute();
        
        if ($stmt->execute()){
            while($row = $stmt->fetch()) {
                $emailEntry = $row['user_email'];
            }
        }
        
        if ($stmt->rowCount() == 1) { // If a row is returned that means a duplicate email address was found.
            $resultMsg = "The email address entered can not be used.  Please try another one.";
            array_push($exportArray, "register_fail", $resultMsg);
        } else {

            try {
                    $sql = "INSERT INTO users (user_username, user_email, user_pass_hash) VALUES (:user_username, :user_email, :user_pass_hash)";
            
                    $sqlPrepare = $conn->prepare($sql);
                    $sqlPrepare->bindParam(':user_username', $usernameToDB);
                    $sqlPrepare->bindParam(':user_email', $inEmail);
                    $sqlPrepare->bindParam(':user_pass_hash', $passToDB);
            } catch (PDOException $e) {
                    $resultMsg = "There was a problem entering the information.  Please try again: " . $e->getMessage();
            }
            $conn = null;
            
            if ($sqlPrepare->execute()){
            
                    $resultMsg = "<h2>Thank You for becoming a member!</h2>";
                    $resultMsg .= "<p>Your account has been setup and is ready to go.";
            } else {
                    $resultMsg = "<h3>A Small Problem Occurred.</h3>";
                    $resultMsg .= "<p>There was an error processing your information.</p>";
                    $resultMsg .= "<p>Please try again.</p>";
            }
            array_push($exportArray, "register_success", $inPass, $inEmail, $usernameToDB, $passToDB, $resultMsg);
        }
    } else {
        array_push($exportArray, "register_fail", $resultEmail, $resultPassword, $resultPasswordConfirm, $resultMsg);
    }

    
    echo json_encode($exportArray);

?>