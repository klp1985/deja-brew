<?php
    session_start();

    $rememberUserCookie = $_COOKIE['rememberUser'];
    if (isset($rememberUserCookie)) {
        $_SESSION['validUser'] = "yes";
    }

    $importArray = [];
    $formResultMsg = '';
    $beerNameMsg = '';
    $breweryNameMsg = '';
    $notesMsg = '';
    $beerID = '';
    $formSet = false;

    if (!isset($_SESSION['validUser']) || $_SESSION['validUser'] != "yes") {        //If user is not valid user return them to homepage where they can login
        header('Location: ../index.html');
    } else {
        
        if (isset($_SESSION['export_array'])) {
            $formSet = true;
            $importArray = $_SESSION['export_array'];
            if ($importArray[0]=="valid_success") {
                $formResultMsg = $importArray[1];
                
            } else if ($importArray[0]=="valid_fail") {
                $beerNameMsg = $importArray[1];
                $breweryNameMsg = $importArray[2];
                $notesMsg = $importArray[3];
            }
            unset($_SESSION['export_array']);
        } 

        $userEmail = $_SESSION['userEmail'];
        $getUsername = explode("@", $userEmail);        //Username is the first part of users email address
        $userName = $getUsername[0];
        $userId = $_SESSION['user_id'];
        $displayList = "";
        
        include 'connectPDO.php';       //Connect to the database

        $sqlList = "SELECT beer_name, beer_brewery, beer_rating, beer_id FROM beer WHERE user_id = :user_id";       //Get users beer info from database based on user ID

        $stmt = $conn->prepare($sqlList);
        $stmt->bindParam(':user_id', $userId);
        $stmt->execute();
        if ($stmt->execute()) {
                $displayList .= "<ul id='beerListElement'>";
            while($row = $stmt->fetch()) {      //Build each row of the table 
                // Place each beer into table
                $displayList .= "<li onclick='viewBeerInfo(".$row['beer_id'].")' class='beerListItem' id='" . str_replace(' ', '', $row['beer_name']) . $row['beer_id'] . "'>";
                $displayList .= "<ul id='beerListContent'>";
                $displayList .= "<div class='row'><li class='beerItemName six columns'>" . $row['beer_name'] . "</li>";
                $displayList .= "<li class='beerItemBrewery six columns'>" . $row['beer_brewery'] . "</li></div>";
                $displayList .= "</ul><!-- end beerListItem -->";
                $displayList .= "</li><!-- end beerListRow -->";
            }
                $displayList .= "</ul> <!-- end beerListElement -->";
                $conn = null;
        }

?>
<!DOCTYPE html>
<html>
  <head>
      <title>Deja Brew</title>

      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
      <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="../vendor/skeleton/normalize.css">
      <link rel="stylesheet" href="../vendor/skeleton/skeleton.css">
      <link rel="stylesheet" type="text/css" href="../css/style.css">

      <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script> 
       
      <script src="../js/capturePicture.js"></script>
      <script src='../js/viewBeerInfo.js'></script>
      <script src='../js/beerToServe.js'></script>

      <script>
            $(document).ready(function() { 

                beerEntry();

<?php if ($formSet) { ?>
                /*$( function() {
                    $( "#dialog-message" ).dialog({
                    modal: true,
                    buttons: {
                        Ok: function() {
                        $( this ).dialog( "close" );
                        }
                    }
                    });
                } );*/
<?php } ?>
                /*
                $( function() {
                    $( "#insertInfo" ).dialog({
                        width: 700,
                        modal: true,
                        autoOpen: false
                    });
                } );
*/
                $('#addBeerBtn').click(function() {     //Functionality for "Add Beer" button; 
                    $('#insertInfo').css('display','block');       //Displays overlay with beer info form  
                });

                $('#closeFormOverlay').click(function() {
                    $('#insertInfo').css('display','none');
                    $('#beerEntryForm input').val("");
                    $('#beerEntryForm textarea').val("");
                    $('input[name="rating"]').prop('checked', false);
                });

                $('.beerListItem').click(function() {     //Functionality for "Add Beer" button; 
                    $('#viewBeerInfo').css('display','block');       //Displays overlay with beer info form  
                });

                $('#closeInfoOverlay').click(function() {
                    $('#viewBeerInfo').css('display','none');
                    $("#viewBeerRating").html("");
                });

                $('#takeImage').click(function() {
                    $('#capImageOverlay').css('display','block');
                    $('#player, #capture').css('display', 'inherit');
                    $('#takeImage, #imageUploadContainer,#orText').css('display', 'none');
                    captureImage();
                });  



                $('#capture').click(function() {
                    $('#capImageOverlay').css('display','none');
                });

                $('#uploadImage').click(function() {
                    $('#takeImage,#orText').css('display','none');
                });

                $('#closeImageCap').click(function() {
                    player.srcObject.getVideoTracks().forEach(track => track.stop());
                    $('#capImageOverlay').css('display','none');
                    $('#imageUploadContainer, #takeImage').css('display','inherit');
                });

                $('#resetForm').click(function() {
                    $('#takeImage, #imageUploadContainer,#orText').css('display', 'inherit');
                    $('#player, #capture, #canvas').css('display', 'none');
                    $('#beerNameMsg, #breweryNameMsg, #ratingMsg, #beerNotesMsg').html('');
                    $('#beerFormSubmit').html('Submit Info');
                });

            });
      </script>

<body>

<div id="dialog-message" title="Form Submit Result">
    <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
    <span id="formResult"><?php echo $formResultMsg;?></span>
</div>

<div class="row">
    <div class="one-third column" id="siteTitle">
        <h1>Deja Brew</h1>
    </div><!-- End siteTitle -->
    <div class="two-thirds column" id="logoutBtnContainer">
        <button type="button" class="button-primary" id="logout-button"><a href="logout.php">Logout</a></button>
    </div><!-- End logoutBtnContainer -->
    <hr>
</div><!-- End row -->


<div class="row">
    <div class="one-fourth column" id="showUsername">
        <img src = "../images/site/beer.png" title="Beer Mug" Alt="Cartoon beer mug" width=7% />
        <h2>Beer-lover on tap:   <span id="userName"><?php echo $userName;?></span></h2>
        
    </div><!-- End four column -->
    <hr>
</div><!-- End row -->
<hr>

<div class="row">
    <h1 id="beerListTitle">Your Beers</h1>
        <div id="addBeerBtnContainer">
            <button type="button" class="button-primary" id="addBeerBtn">Add Beer!</button>
        </div><!-- End addBeerBtnContainer -->
</div><!-- End row -->

<div class="row" id="listHeading">
    <h2 class="six columns" id="listHeadingBeer">Beer Name</h2>
    <h2 class="six columns" id="listHeadingBrewery">Brewery Name</h2>
</div><!-- End row -->
        
<div id="beerList">
    <?php echo $displayList;?>
</div><!-- End beerList -->

<div id="insertInfo">
<form action="handleBeerInput.php" method="post" enctype="multipart/form-data" id="beerEntryForm">
  <div id="closeFormOverlay"><h3 class="closeOverlayX">[ X ]</h3></div>
  <div id="formText">
      <h3>Add Beer</h3>
    
    <div id="beerNameInput">
        <p>Beer Name:</p><span id="beerNameMsg"></span>
        <input type="text" size="30" name = "beerName" id="beerName">
    </div>
    <div id="breweryNameInput">
        <p>Brewery:</p><span id="breweryNameMsg"></span>
        <input type="text" size="30" name = "breweryName" id="breweryName">
    </div>
    <div id="beerRatingInput" class="beerRating">
        <p>Rating:</p><span id="ratingMsg"></span>
            <input class="rating beerRate-5" id="rating-5" type="radio" name="rating" value="5" />
            <label class="rating beerRate-5" for="rating-5"></label>
            <input class="rating beerRate-4" id="rating-4" type="radio" name="rating"  value="4"/>
            <label class="rating beerRate-4" for="rating-4"></label>
            <input class="rating beerRate-3" id="rating-3" type="radio" name="rating"  value="3"/>
            <label class="rating beerRate-3" for="rating-3"></label>
            <input class="rating beerRate-2" id="rating-2" type="radio" name="rating"  value="2"/>
            <label class="rating beerRate-2" for="rating-2"></label>
            <input class="rating beerRate-1" id="rating-1" type="radio" name="rating"  value="1"/>
            <label class="rating beerRate-1" for="rating-1"></label>
    </div>
    <div id="beerNotesInput">
        <p>Notes:</p><span id="beerNotesMsg"></span>
        <textarea rows="6" cols="30" name = "beerNotes" id="beerNotes"></textarea>
    </div>
      <h5>Image:</h5>
      <div id="imageCapBtnContainer">
          <input type="hidden" name="captureImagePath" id="captureImagePath" >
          <button type="button" id="takeImage">Take Picture</button>          
      </div>
    
      <div id="capImageOverlay">
        <div id="capImageOverlayItems">
            <button type="button" class="button-primary" id="closeImageCap">Cancel</button>
            <video id="player" autoplay></video>
            <button type="button" class="button-primary" id="capture">Capture</button>
        </div>
      </div> <!-- End capImageOverlay -->
      <canvas id="canvas" width=360 height=480></canvas>
      
      <h4 id="orText">Or</h4>

      <div id="imageUploadContainer">
        Select image to upload:
        <input type="file" name="uploadImage" id="uploadImage">
      </div>  
    
    <button type="button" id="beerFormSubmit" class="button" name="sendForm">Submit Info</button>
    <!--<input type="submit" value="Submit Info" name="submit">-->
    <button type="reset" value="Reset" id="resetForm">Reset</button>

        </div>
    </form>
    </div>

    <div id='viewBeerInfo'>
        <div id='beerInfoText'>
            <button id="closeInfoOverlay" class="button button-primary"><h4 class="closeOverlayX">[ X ]</h4></button>
            <h4>Beer: <span id="viewBeerName"></span></h4>
            <h3>Rating: <span id="viewBeerRating"></span></h3>
            <div id="beerImg"><img src='' id="viewBeerImage" width="100%"/></div>
            <h4>Brewery: <span id="viewBreweryName"></span></h4>
            <h5>Notes:</h5>
            <p id="viewBeerNotes"></p> 
            <button class="delete-button button button-primary"><a href='' id="deleteBtn">Delete</a></button>
        </div>
    </div>
    
</body>
</html>
<?php       
    }    //End else
?>