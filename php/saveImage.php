<?php
	session_start();
	$exportArray = [];
	$resultMsg = '';
	$username = $_SESSION['username'];
	$upload_dir = "../images/user_images/".$username."/";
	
	if (!is_dir($upload_dir)){
                  mkdir($upload_dir);
        }
	
	// make sure the image-data exists and is not empty
	// xampp is particularly sensitive to empty image-data 
	if ( isset($_POST["image"]) && !empty($_POST["image"]) ) {    

    	// get the dataURL
    	$dataURL = $_POST["image"];  

    	// the dataURL has a prefix (mimetype+datatype) 
    	// that we don't want, so strip that prefix off
   	$parts = explode(',', $dataURL);  
    	$data = $parts[1];  

    	// Decode base64 data, resulting in an image
    	$data = base64_decode($data);  

    	// create a temporary unique file name
    	$file = $upload_dir. time() . '.png';

    	// write the file to the upload directory
    	$success = file_put_contents($file, $data);
    	
    	
	if($success) {
		array_push($exportArray, "image_success", $file);
	} else {
		$resultMsg = "Unable to save this image.  Please try again";
		array_push($exportArray, "image_fail", $resultMsg);
	}
}
	echo json_encode($exportArray);
?>