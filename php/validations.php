<?php

				class Validations {

				//Properties
						public $name;
						public $email;
						public $subject;
						public $userMsg;
						public $phone;
						public $price;
						public $username;
						public $password;
						public $passwordConfirm;
						public $userRole;
						public $productType;
						public $validForm;


				//Setters



						function set_name($inName) {

							//this -->  refers to the current object
							// -> go out to the current object and get the property.  No $ in front of property.

								$this -> name = $inName;  //Put input value into property

						}


						function set_email($inEmail) {

								$this -> email = $inEmail;  //Put input value into property

						}


						function set_subject($inSubject) {

								$this -> subject = $inSubject;

						}


						function set_userMsg($inUserMsg) {

								$this -> userMsg = $inUserMsg;

						}

						function set_phone($inPhone) {

								$this -> phone = $inPhone;

						}

						function set_productType($inProductType) {

								$this -> productType = $inProductType;

						}

						function set_price($inPrice) {

								$this -> price = $inPrice;

						}

						function set_username($inUsername) {
							$this -> username = $inUsername;
						}

						function set_password($inPassword) {
							$this -> password = $inPassword;
						}

						function set_passwordConfirm($inPasswordConfirm) {
							$this -> passwordConfirm = $inPasswordConfirm;
						}

						function set_userRole($inUserRole) {
							$this -> userRole = $inUserRole;
						}

						function set_validForm($inValidForm) {
								$this -> validForm = $inValidForm;
						}


				//Getters
						function get_name() {

								return $this -> name;

						}


						function get_email() {

								return $this -> email;

						}


						function get_subject() {

								return $this -> subject;

						}


						function get_userMsg() {

								return $this -> userMsg;

						}


						function get_phone() {

								return $this -> phone;

						}

						function get_productType() {

								return $this -> productType;

						}

						function get_price() {

								return $this -> price;

						}

						function get_username() {
								return $this -> username;
						}

						function get_password() {
								return $this -> password;
						}

						function get_passwordConfirm () {
								return $this -> passwordConfirm;
						}

						function get_userRole () {
								return $this -> userRole;
						}

						function get_validForm() {
								return $this -> validForm;
						}



				//Processing
						function validateName() {

								$nameErrMsg = "";

								if ( !$this->name == "") {
										$inName = ltrim($this->name);

										$inName = filter_var($inName, FILTER_SANITIZE_STRING);

										$this->name = $inName;

										return $nameErrMsg;

								}

								else {
										$this->validForm = false;

										$nameErrMsg = "Your name is required.";

										return $nameErrMsg;
								}
						}



						function validateEmail() {

								$emailErrMsg = "";

								if ( !$this->email == "") {

										$inEmail = filter_var($this->email, FILTER_SANITIZE_EMAIL);

										$this -> email = $inEmail;

										if (filter_var($inEmail, FILTER_VALIDATE_EMAIL) === false) {

												$this->validForm = false;

												$emailErrMsg = "Email address is not formatted correctly.  Use the format jdoe@example.com";

												return $emailErrMsg;

										}

									return $emailErrMsg;

								}

								else {
										$this->validForm = false;

										$emailErrMsg = "Email address is required.";

										return $emailErrMsg;
								}
						}



						function validateSubject() {

								$subjectErrMsg = "";

								$userMsgErrMsg = "";

								if ($this->subject == "default") {

										$this->validForm = false;

										$subjectErrMsg = "Please select a reason for contact.";

										return $subjectErrMsg;
								}
						}



						function validateUserMsg() {

								$userMsgErrMsg = "";

								if ($this -> userMsg == "") {

										if ($this->subject == "other") {
												$this->validForm = false;

												$userMsgErrMsg = "A comment is required for the reason selected.";

												return $userMsgErrMsg;
										}
								} else {

										$userMsg = filter_var($this->userMsg, FILTER_SANITIZE_STRING);

										$this->userMsg = $userMsg;

								}
						}


						function validateResponseMsg() {

								$userMsgErrMsg = "";

								if ($this -> userMsg == "") {

												$this->validForm = false;

												$userMsgErrMsg = "Response is required.";

												return $userMsgErrMsg;

								}
						}

						function validateProductDescription() {

								$userMsgErrMsg = "";

								if ($this -> userMsg == "") {

												$this->validForm = false;

												$userMsgErrMsg = "Product description is required.";

												return $userMsgErrMsg;

								}
						}


						function validatePhauxn() {

								if (!$this->phone == "") {

										$this->validForm = false;


								}
						}


						function validatePrice() {
							$priceErrMsg = "";

								if (!$this->price == "") {
									if(preg_match('^[0-9]+\.[0-9]{2}$^', $this->price)) {
											$this->price = filter_var($this->price, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
									} else {
										$this->validForm = false;
										$priceErrMsg = "A price must be a number with 2 decimal places.";
										return $priceErrMsg;
									}
								} else {
										$this->validForm = false;
										$priceErrMsg = "A price must be entered.";
										return $priceErrMsg;
								}
						}


						function validateProductType() {

								$productTypeErrMsg = "";

								if (!isset($this -> productType)) {

												$this->validForm = false;

												$productTypeErrMsg = "A product type must be selected.";

												return $productTypeErrMsg;

								}
						}


						function validateUsername() {
								$usernameErrMsg = "";

								if (!$this->username == "" ) {
									if(preg_match('/^\w{5,}$/', $this->username)) {
											$this->username = filter_var($this->username, FILTER_SANITIZE_STRING);
									} else {
										$this->validForm = false;
										$usernameErrMsg = "Username must be at least 5 characters and may only contain letters or numbers.";

										return $usernameErrMsg;
									}
								} else {
										$this->validForm = false;
										$usernameErrMsg = "You must enter a username!";

										return $usernameErrMsg;
								}

						}


						function validatePassword() {
								$passwordErrMsg = "";

								if (!$this->password == "" ) {
										$uppercase = preg_match('@[A-Z]@', $this->password);
										$lowercase = preg_match('@[a-z]@', $this->password);
										$number    = preg_match('@[0-9]@', $this->password);

										if(!$uppercase || !$lowercase || !$number || strlen($this->password) < 6) {
												$this->validForm = false;

												$passwordErrMsg = "Password must be at least 6 characters and include a uppercase and lowercase letter as well as a number.";

												return $passwordErrMsg;
										}

								} else {
										$this->validForm = false;

										$passwordErrMsg = "You must enter a password!";

										return $passwordErrMsg;
								}

						}


						function validatePasswordConfirm() {
								$passwordConfirmErrMsg = "";

								if (!$this->passwordConfirm == "" ) {
									if ($this->password != $this->passwordConfirm) {
											$this->validForm = false;

											$passwordConfirmErrMsg = "Please make sure your password matches in both fields.";

											return $passwordConfirmErrMsg;
									}

								} else {
										$this->validForm = false;

										$passwordConfirmErrMsg = "You must verify your password!";

										return $passwordConfirmErrMsg;
								}

						}


						function validateUserRole() {

								$userRoleErrMsg = "";

								if ($this -> userRole < 1 || $this -> userRole > 3) {

												$this->validForm = false;

												$userRoleErrMsg = "A user role must be selected.";

												return $userRoleErrMsg;

								}
						}
				}

?>
